import random 
from tensorflow.keras import backend as K
from tensorflow.python.keras.layers import Layer
from tensorflow.keras.initializers import RandomUniform, Initializer, Orthogonal, Constant
import numpy as np

class InitTranslationRandom(Initializer):
    """ Initializer for initialization of centers of Wlt network
        as random samples from the given data set.
    # Arguments
        X: matrix, dataset to choose the centers from (random rows 
          are taken as centers)
    """
    def __init__(self, X):
        self.X = X 

    def __call__(self, shape, dtype=None):
        assert shape[1] == self.X.shape[1]
        idx = np.random.randint(self.X.shape[0], size=shape[0])
        return self.X[idx,:]

class InitTranslationConstant(Initializer):
    """ Initializer for initialization of translation of Wlt network
        as constant heuristics from the given data set.

    # Arguments
        X: matrix, dataset to choose the translation from 
    """
    def __init__(self, X):
        self.X = X 

    def __call__(self, shape, dtype=None):
        assert shape[1] == self.X.shape[1]
        Max=np.max(self.X,axis=0)
        Min=np.min(self.X,axis=0)
        #import pdb; pdb.set_trace()
        C=[]
        for i in range(shape[1]):
            c=np.zeros(shape[0])+0.5*(Min[i]+Max[i])
            C.append(c)
        return np.reshape(C, shape)

class InitTranslationSpace(Initializer):
    """ Initializer for initialization of translation of Wlt network
        as uniform space from the given data set.

    # Arguments
        X: matrix, dataset to choose the centers from
    """
    def __init__(self, X):
        self.X = X 

    def __call__(self, shape, dtype=None):
        assert shape[1] == self.X.shape[1]
        Max=np.max(self.X,axis=0)
        Min=np.min(self.X,axis=0)
        #import pdb; pdb.set_trace()
        C=[]
        for i in range(shape[1]):
            c=np.linspace(Min[i],Max[i],num=shape[0])
            C.append(c)
        return np.reshape(C, shape)
        
class InitScalesConstant(Initializer):
    """ Initializer for initialization of scales of Wlt network
        as constant heuristics from the given data set.

    # Arguments
        X: matrix, dataset to choose the scales from 
    """
    def __init__(self, X):
        self.X = X 

    def __call__(self, shape, dtype=None):
        assert shape[1] == self.X.shape[1]
        Max=np.max(self.X,axis=0)
        Min=np.min(self.X,axis=0)
        #import pdb; pdb.set_trace()
        C=[]
        for i in range(shape[1]):
            c=np.zeros(shape[0])+0.2*(Max[i]-Min[i])
            C.append(c)
        return np.reshape(C, shape)

class WaveletLayer(Layer):
    """ Layer of Wavelet units. 

    # Example
 
    ```python
        model = Sequential()
        model.add(RBFLayer(10,
                           initializer=InitCentersRandom(X), 
                           input_shape=(1,)))
        model.add(Dense(1))
    ```
    

    # Arguments
        output_dim: number of hidden units (i.e. number of outputs of the layer)
        initializer: instance of initiliazer to initialize weight

    """
    def __init__(self, output_dim, translation_initializer=None,
                 scales_initializer=None, **kwargs):
        self.output_dim = output_dim
        if not translation_initializer:
            self.translation_initializer = Constant(value=0.5)
            #self.initializer = Orthogonal()
        else:
            self.translation_initializer = translation_initializer
        if not scales_initializer:
            self.scales_initializer = Constant(value=0.2)
            #self.initializer = Orthogonal()
        else:
            self.scales_initializer = scales_initializer 
        super(WaveletLayer, self).__init__(**kwargs)

    def build(self, input_shape):
        
        self.translations = self.add_weight(name='translations',
                                            shape=(self.output_dim, input_shape[1]),
                                            initializer=self.translation_initializer,
                                            trainable=True)
        self.scales = self.add_weight(name='scales',
                                      shape=(self.output_dim,input_shape[1]),
                                      initializer=self.scales_initializer,
                                      #initializer='ones',
                                      trainable=True)
            
        super(WaveletLayer, self).build(input_shape)

    def call(self, x):
        KSI = K.expand_dims(self.translations)
        DZETA=K.expand_dims(self.scales)
        Z = K.transpose((KSI-K.transpose(x)/DZETA)) 

        return K.prod(Z*K.exp(-0.5*Z**2),axis=1)

    def compute_output_shape(self, input_shape):
        return (input_shape[0], self.output_dim)


    def get_config(self):
        # have to define get_config to be able to use model_from_json
        config = {
            'output_dim': self.output_dim
        }
        base_config = super(WaveletLayer, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))
